# FoodFinder
### Developed by Gabriel Beltrame Silva.

## Project details:
 - XCode 10.2.1.
 - iOS 12 or higher.

## Architecture: Why MVI?
Reactive programming can make stuff complicated if you have multiple event streams from Core to UI and vice versa.
MVI helps streamline stuff by providing a single data flow. (Command -> Mutation -> State)

## Why Cocoapods?
Because it's better than manually setting up dependencies. Also, I didn't have time to learn how to use the Swift Package Manager.

## Which Pods I used and why: 
 - 'Alamofire'
     - I didn't want to make NSURLRequests manually.
 - 'Rx'
    - Reactive programming saves me a lot of time. 

## Things I plan on doing:

- ~~Show closest restaurants on the map.~~
- ~~Search Restaurants.~~
- ~~Option to call the restaurant.~~
- Sort Search Results.
- Show restaurant information.
- ~~Infinite scroll on the search.~~
- Treat error when the user deny the location request.
- ~~Loading indicator.~~
- Choosing location on the map.
- Add restaurants to favorite list.
- Think of new functionalities.