//
//  SearchResult.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

struct SearchResult: Equatable, Decodable {
    var resultsFound: Int
    var resultsOffset: Int
    var itemsPerPage: Int
    var restaurants: [Restaurant]

    enum CodingKeys: String, CodingKey {
        case resultsFound = "results_found"
        case resultsOffset = "results_start"
        case itemsPerPage = "results_shown"
        case restaurants = "restaurants"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        resultsFound = try container.decode(Int.self, forKey: .resultsFound)
        resultsOffset = try container.decode(Int.self, forKey: .resultsOffset)
        itemsPerPage = try container.decode(Int.self, forKey: .itemsPerPage)
        let nestedRestaurant = try container.decode([NestedRestaurant].self, forKey: .restaurants)
        restaurants = nestedRestaurant.map({ $0.restaurant })
    }
}
