//
//  NestedRestaurant.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

// The Zomato API returns an array of dictionaries instead of an array of objects.
// To correctly Decode the value of the requests I'm forced to use theis struct.
struct NestedRestaurant: Equatable, Codable {
    var restaurant: Restaurant
    
    enum CodingKeys: String, CodingKey {
        case restaurant
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        restaurant = try container.decode(Restaurant.self, forKey: .restaurant)
    }
}
