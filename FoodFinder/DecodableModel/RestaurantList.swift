//
//  RestaurantList.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

struct RestaurantList: Equatable, Decodable {
    var nearbyRestaurants: [Restaurant]
    
    enum CodingKeys: String, CodingKey {
        case restaurant = "restaurant"
        case nearbyRestaurants = "nearby_restaurants"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedRestaurant = try container.decode([NestedRestaurant].self, forKey: .nearbyRestaurants)
        nearbyRestaurants = nestedRestaurant.map({ $0.restaurant })
    }
}
