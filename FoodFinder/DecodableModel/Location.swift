//
//  Location.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

struct Location: Equatable, Codable {
    let address: String
    let locality: String
    let city: String
    let latitude: String
    let longitude: String
    let zipcode: String
    
    enum CodingKeys: String, CodingKey {
        case address
        case locality
        case city
        case latitude
        case longitude
        case zipcode
    }
}
