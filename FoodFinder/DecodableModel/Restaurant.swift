//
//  Restaurant.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

struct Restaurant: Equatable, Codable {
    let id: String
    let name: String
    let url: String
    let location: Location
    let averageCostForTwo: Int
    let priceRange: Int
    let currency: String
    let thumb: String
    let featuredImage: String
    let phoneNumbers: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case url
        case location
        case averageCostForTwo = "average_cost_for_two"
        case priceRange = "price_range"
        case currency
        case thumb
        case featuredImage = "featured_image"
        case phoneNumbers = "phone_numbers"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        url = try container.decode(String.self, forKey: .url)
        location = try container.decode(Location.self, forKey: .location)
        averageCostForTwo = try container.decode(Int.self, forKey: .averageCostForTwo)
        priceRange = try container.decode(Int.self, forKey: .priceRange)
        currency = try container.decode(String.self, forKey: .currency)
        thumb = try container.decode(String.self, forKey: .thumb)
        featuredImage = try container.decode(String.self, forKey: .featuredImage)
        
        if let csvNumbers = try container.decodeIfPresent(String.self, forKey: .phoneNumbers),
            csvNumbers != "Not available for this place" {
            phoneNumbers = csvNumbers.split(separator: ",").map({ String($0) })
        } else {
            phoneNumbers = nil
        }
    }
    
}
