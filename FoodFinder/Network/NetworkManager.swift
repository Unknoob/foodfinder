//
//  NetworkManager.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Alamofire
import RxAlamofire
import RxSwift

/// The class responsible for all network requests.
public class NetworkManager {
    
    // MARK: - Constants
    
    /// The base URL for the API calls.
    private static let kBaseURL: String = "https://developers.zomato.com/api/v2.1/"
    private static let authenticationHeader = ["user-key": "0bd73f29d3dfb57bbcc674020834de9e"]
    
    // MARK: - Instance Methods
    
    static func apiCall<T>(requestPath: String, parameters: JSONDictionary, httpMethod: Alamofire.HTTPMethod) -> Observable<Result<T>> where T: Decodable {

        return RxAlamofire.request(httpMethod, kBaseURL + requestPath, parameters: parameters, headers: authenticationHeader)
            .responseJSON()
            .map { response in
                switch response.result {
                case .success:
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Result: \(response.result)")
                    
                    if let statusCode = response.response?.statusCode, statusCode != 200 {
                        return .failure(NetworkError.errorWithCode(statusCode))
                    }

                    guard let data = response.data else {
                        return .failure(MessageError.message(title: "Parsing Error", message: "Empty Data"))
                    }
                    do {
                        print("JSON: \(try! JSONSerialization.jsonObject(with: data, options: .allowFragments))")
                        let model: T = try JSONDecoder().decode(T.self, from: data)
                        return .success(model)
                    } catch let DecodingError.typeMismatch(type, context)  {
                        return .failure(MessageError.message(title: context.debugDescription, message: "Type '\(type)'\nCodingPath: \(context.codingPath)"))
                    } catch let DecodingError.keyNotFound(key, context) {
                        return .failure(MessageError.message(title: context.debugDescription, message: "Key '\(key)' not found"))
                    } catch let error {
                        return .failure(MessageError.message(title: "Unknown Error", message: error.localizedDescription))
                    }

                case .failure(let error):
                    return .failure(NetworkError.errorWithCode(error.code))
                }
                
            }
    }
    
}
