//
//  SearchRestaurantFetcher.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Alamofire
import CoreLocation
import RxSwift

protocol SearchRestaurantFetching {
    func searchRestaurants(query: String, offset: Int, itemsPerPage: Int) -> Observable<Result<SearchResult>>
}

class SearchRestaurantFetcher: SearchRestaurantFetching {
    func searchRestaurants(query: String, offset: Int, itemsPerPage: Int) -> Observable<Result<SearchResult>> {
        return NetworkManager.apiCall(requestPath: "search",
                                      parameters: ["q": query,
                                                   "start": offset,
                                                   "count": itemsPerPage],
                                      httpMethod: Alamofire.HTTPMethod.get)
    }
}
