//
//  RestaurantListInteractor.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import RxSwift

protocol RestaurantListInteracting {
    var initialState: RestaurantListState { get set }
    var dataFetcher: SearchRestaurantFetching { get }
    func execute(command: Observable<RestaurantListCommand>) -> Observable<RestaurantListState>
    func mutation(for command: RestaurantListCommand) -> Observable<RestaurantListMutation>
    func reduce(state: RestaurantListState, mutation: RestaurantListMutation) -> RestaurantListState
}

class RestaurantListInteractor: RestaurantListInteracting {
    var initialState: RestaurantListState
    var dataFetcher: SearchRestaurantFetching
    
    let kItemsPerPage: Int = 20
    var nextOffset: Int = 0
    var query: String = ""
    
    init(initialState: RestaurantListState, dataFetcher: SearchRestaurantFetching) {
        self.initialState = initialState
        self.dataFetcher = dataFetcher
    }
    
    func execute(command: Observable<RestaurantListCommand>) -> Observable<RestaurantListState> {
        let lastCommand = ReplaySubject<RestaurantListCommand>.create(bufferSize: 1)
        let lastState = ReplaySubject<RestaurantListState>.create(bufferSize: 1)
        
        return command
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .flatMap { command -> Observable<RestaurantListMutation> in
                return self.mutation(for: command)
                    .do(onNext: { _ in
                        lastCommand.onNext(command)
                    })
            }
            .scan(initialState, accumulator: reduce)
            .startWith(initialState)
            .do(onNext: lastState.onNext)
    }
    
    func mutation(for command: RestaurantListCommand) -> Observable<RestaurantListMutation> {
        switch command {
        case let.call(phone):
            if let url = URL(string: "tel://\(phone.removingPunctuation())") {
                UIApplication.shared.open(url)
            }
            return .empty()
        case let .searchRestaurants(query):
            self.query = query
            return dataFetcher.searchRestaurants(query: query, offset: 0, itemsPerPage: kItemsPerPage)
                .map({ result -> RestaurantListMutation in
                    switch result {
                    case let .success(restaurantList):
                        return RestaurantListMutation.searchSuccess(restaurantList)
                    case let .failure(error):
                        return RestaurantListMutation.error(error)
                    }
                })
                .startWith(RestaurantListMutation.loading)
        case .loadNextPage:
            return dataFetcher.searchRestaurants(query: query, offset: nextOffset, itemsPerPage: kItemsPerPage)
                .map({ result -> RestaurantListMutation in
                    switch result {
                    case let .success(restaurantList):
                        return RestaurantListMutation.pageLoadSuccess(restaurantList)
                    case let .failure(error):
                        return RestaurantListMutation.error(error)
                    }
                })
                .startWith(RestaurantListMutation.loading)
        }
    }
    
    func reduce(state: RestaurantListState, mutation: RestaurantListMutation) -> RestaurantListState {
        var newState = state
        newState.isLoading = false
        newState.error = nil
        switch mutation {
        case let .searchSuccess(searchResult):
            let nextOffset = searchResult.resultsOffset + searchResult.itemsPerPage
            newState.restaurants = [searchResult.restaurants]
            newState.resultsOffset = nextOffset
            newState.resultsFound = searchResult.resultsFound
            self.nextOffset = nextOffset
        case let .pageLoadSuccess(searchResult):
            let nextOffset = searchResult.resultsOffset + searchResult.itemsPerPage
            newState.restaurants.append(searchResult.restaurants)
            newState.resultsOffset = nextOffset
            newState.resultsFound = searchResult.resultsFound
            self.nextOffset = nextOffset
        case let .error(error):
            newState.error = error
        case .loading:
            newState.isLoading = true
        }
        
        return newState
    }
}
