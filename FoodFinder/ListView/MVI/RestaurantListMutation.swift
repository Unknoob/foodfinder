//
//  RestaurantListMutation.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

enum RestaurantListMutation: Equatable {
    case searchSuccess(SearchResult)
    case pageLoadSuccess(SearchResult)
    case error(GenericError)
    case loading
    
    static func == (lhs: RestaurantListMutation, rhs: RestaurantListMutation) -> Bool {
        switch (lhs, rhs) {
        case (let .searchSuccess(lhsSearchResult), let .searchSuccess(rhsSearchResult)):
            return lhsSearchResult == rhsSearchResult
        case (let .pageLoadSuccess(lhsSearchResult), let .pageLoadSuccess(rhsSearchResult)):
            return lhsSearchResult == rhsSearchResult
        case (let .error(lhsError), let .error(rhsError)):
            return lhsError.message == rhsError.message
        case (.loading, .loading):
            return true
        default:
            return false
        }
    }
}
