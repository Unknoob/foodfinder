//
//  RestaurantListCommand.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import CoreLocation

enum RestaurantListCommand: Equatable {
    case call(String)
    case searchRestaurants(String)
    case loadNextPage
}
