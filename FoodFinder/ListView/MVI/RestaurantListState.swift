//
//  RestaurantListState.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

struct RestaurantListState: Equatable {
    var resultsOffset: Int = 0
    var resultsFound: Int = 0
    var restaurants: [[Restaurant]] = [[]]
    var error: GenericError?
    var isLoading: Bool
    
    let kMaxItems: Int = 100
    
    var hasNextPage: Bool {
        return (resultsOffset < resultsFound) && (resultsOffset < kMaxItems)
    }
    
    init() {
        self.isLoading = true
    }
    
    static func == (lhs: RestaurantListState, rhs: RestaurantListState) -> Bool {
        return (lhs.restaurants == rhs.restaurants) && (lhs.isLoading == rhs.isLoading)
    }
}
