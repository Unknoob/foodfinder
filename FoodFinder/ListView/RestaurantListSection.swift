//
//  RestaurantListSection.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import RxCocoa
import RxDataSources
import RxSwift

struct RestaurantListSection: RxDataSources.AnimatableSectionModelType {
    typealias Identity = Int
    
    var identity: Int {
        return items.reduce(0, { (partialResult, nextItem) -> Int in
            return (partialResult << 5) &+ partialResult &+ nextItem.identity
        })
    }
    
    typealias Item = RestaurantListItem
    let items: [Item]
    
    init(items: [Item]) {
        self.items = items
    }
    
    init(original: RestaurantListSection, items: [Item]) {
        self.init(items: items)
    }
    
    static func ==(lhs: RestaurantListSection, rhs: RestaurantListSection) -> Bool {
        return lhs.items == rhs.items
    }
    
}

enum RestaurantListItem: Equatable {
    case restaurantCell(Restaurant)
    case loadingCell
}

extension RestaurantListItem: IdentifiableType {
    
    typealias Identity = Int
    
    var identity: Int {
        switch self {
        case let .restaurantCell(restaurant):
            return restaurant.id.hashValue
        case .loadingCell:
            return 0
        }
    }
    
}
