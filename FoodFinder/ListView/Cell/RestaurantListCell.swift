//
//  RestaurantListCell.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import AlamofireImage
import UIKit

class RestaurantListCell: UITableViewCell {
    @IBOutlet weak var featuredImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    func configure(for restaurant: Restaurant) {
        if let imageURL = URL(string: restaurant.featuredImage) {
            featuredImageView.af_setImage(withURL: imageURL, placeholderImage: UIImage(named: "placeholder"), imageTransition: UIImageView.ImageTransition.crossDissolve(0.2))
        } else if let imageURL = URL(string: restaurant.thumb) {
            featuredImageView.af_setImage(withURL: imageURL, placeholderImage: UIImage(named: "placeholder"), imageTransition: UIImageView.ImageTransition.crossDissolve(0.2))
        } else {
            featuredImageView.image = UIImage(named: "placeholder")
        }
        nameLabel.text = restaurant.name
        addressLabel.text = restaurant.location.address
        priceLabel.text = getPriceRangeString(from: restaurant)
        
        if restaurant.phoneNumbers != nil {
            accessoryType = .disclosureIndicator
        } else {
            accessoryType = .none
        }
    }
    
    private func getPriceRangeString(from restaurant: Restaurant) -> String {
        return "Price: \(String(repeating: "$", count: restaurant.priceRange))"
    }
}
