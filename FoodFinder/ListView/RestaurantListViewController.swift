//
//  RestaurantListViewController.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Alamofire
import RxCocoa
import RxDataSources
import RxSwift
import UIKit

class RestaurantListViewController: UIViewController, ErrorMessageDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    let disposeBag = DisposeBag()
    
    private(set) public lazy var inputSubject = PublishSubject<RestaurantListState>()
    private(set) public lazy var outputSubject = PublishSubject<RestaurantListCommand>()
    private let shouldLoadMore = BehaviorSubject<Bool>(value: false)
    
    private(set) var dataSource = RxTableViewSectionedAnimatedDataSource<RestaurantListSection>(configureCell: { (_, _, _, _) in return UITableViewCell(frame: .zero) })
    
    var searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        searchController.searchBar.backgroundColor = .white
        searchController.searchBar.placeholder = "Search Restaurants"
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
        
        setupBindings()
    }
    
    private func setupBindings() {
        searchController.searchBar.rx.searchButtonClicked
            .do(onNext: { [weak self] _ in
                self?.searchController.dismiss(animated: true)
            })
            .withLatestFrom(searchController.rx.text)
            .filterNil()
            .filter({ $0.isNotEmpty })
            .distinctUntilChanged()
            .map({ query -> RestaurantListCommand in
                return RestaurantListCommand.searchRestaurants(query)
            })
            .bind(to: outputSubject)
            .disposed(by: disposeBag)
        
        tableView.rx.reachedBottom
            .withLatestFrom(shouldLoadMore)
            .filter({ $0 })
            .map({ _ -> RestaurantListCommand in
                return RestaurantListCommand.loadNextPage
            })
            .bind(to: outputSubject)
            .disposed(by: disposeBag)
        
        let interactor = RestaurantListInteractor(initialState: RestaurantListState(), dataFetcher: SearchRestaurantFetcher())
        
        outputSubject
            .bind(to: interactor.execute)
            .bind(to: inputSubject)
            .disposed(by: disposeBag)
        
        inputSubject
            .bind(to: configureSections)
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        dataSource.animationConfiguration = AnimationConfiguration(insertAnimation: .middle,
                                                                   reloadAnimation: .middle,
                                                                   deleteAnimation: .middle)
        
        dataSource.configureCell = { (dataSource, tableView, indexPath, item) in
            switch item {
            case let .restaurantCell(restaurant):
                let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantListCell", for: indexPath) as! RestaurantListCell
                cell.configure(for: restaurant)
                return cell
            case .loadingCell:
                let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
                return cell
            }
            
        }
        
        tableView.rx.itemSelected
            .bind(to: tableView.rx.deselectRowAtIndexPath(animated: true))
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(RestaurantListItem.self)
            .map({ listItem -> [String]? in
                guard
                    case let .restaurantCell(restaurant) = listItem
                else {
                    return nil
                }
                return restaurant.phoneNumbers
            })
            .filterNil()
            .bind(to: showAlertController)
            .bind(to: outputSubject)
            .disposed(by: disposeBag)
    }
    
    private func registerCells() {
        tableView.register(RestaurantListCell.self)
        tableView.register(LoadingCell.self)
    }
    
    private func configureSections(forState state: Observable<RestaurantListState>) -> Observable<[RestaurantListSection]> {
        return state
            .do(onNext: { [weak self] state in
                if let error = state.error {
                    self?.showErrorMessage(title: "Error", message: error.message)
                }
                self?.shouldLoadMore.onNext(state.hasNextPage)
            })
            .map({ (state) -> [RestaurantListSection] in
                var restaurantSections: [RestaurantListSection] = []
                
                state.restaurants.forEach({ restaurantPage in
                    var restaurantListItems: [RestaurantListItem] = []
                    restaurantPage.forEach({ restaurant in
                        restaurantListItems.append(RestaurantListItem.restaurantCell(restaurant))
                    })
                    if restaurantListItems.isNotEmpty {
                        restaurantSections.append(RestaurantListSection(items: restaurantListItems))
                    }
                })
                
                if state.hasNextPage {
                    restaurantSections.append(RestaurantListSection(items: [RestaurantListItem.loadingCell]))
                } else if restaurantSections.isEmpty && state.isLoading {
                    restaurantSections.append(RestaurantListSection(items: [RestaurantListItem.loadingCell]))
                }
                
                return restaurantSections
            })
    }
    
    private func showAlertController(forPhones phones: Observable<[String]>) -> Observable<RestaurantListCommand> {
        
        return phones.flatMap({ phones -> Observable<String?> in
            return Observable.create { [weak self] observer in
                let alertController = UIAlertController(title: "Call Restaurant", message: "Select a number to call", preferredStyle: .actionSheet)
                
                phones.forEach({ phone in
                    let phoneAction = UIAlertAction(title: phone, style: .default, handler: { _ in
                        observer.onNext(phone)
                        observer.onCompleted()
                    })
                    alertController.addAction(phoneAction)
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                    observer.onNext(nil)
                    observer.onCompleted()
                })
                alertController.addAction(cancelAction)
                
                self?.present(alertController, animated: true, completion: nil)
                return Disposables.create {
                    alertController.dismiss(animated: true, completion: nil)
                }
            }
        })
        .filterNil()
        .map({ phone -> RestaurantListCommand in
            return RestaurantListCommand.call(phone)
        })
        
        
        
    }
    
//    static func showAlertController(title: String, message: String, preferredStyle: UIAlertControllerStyle = .Alert, animated: Bool = true, actions: [Action]) -> Observable<Result> {
//
//    }

}
