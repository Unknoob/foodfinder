//
//  MapViewController.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import CoreLocation
import MapKit
import RxCocoa
import RxCoreLocation
import RxMKMapView
import RxOptional
import RxSwift
import UIKit

class MapViewController: UIViewController, ErrorMessageDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    let disposeBag = DisposeBag()
    
    private(set) public lazy var inputSubject = PublishSubject<MapState>()
    private(set) public lazy var outputSubject = PublishSubject<MapCommand>()
    
    private let locationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    
    // The distance we need to move to trigger a new restaurant load
    private let kDistanceTolerance = 20.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsUserLocation = true
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        setupBindings()
    }
    
    private func setupBindings() {
        let interactor = MapInteractor(initialState: MapState(),
                                       dataFetcher: LocationRestaurantFetcher())
        
        outputSubject
            .bind(to: interactor.execute)
            .bind(to: inputSubject)
            .disposed(by: disposeBag)
        
        inputSubject
            .bind(to: configureAnnotations)
            .asDriver(onErrorJustReturn: [])
            .drive(mapView.rx.annotations)
            .disposed(by: disposeBag)
        
        locationManager.rx.location
            .filterNil()
            .filter({ [weak self] location -> Bool in
                guard let self = self else {
                    return false
                }
                
                guard let lastLocation = self.lastLocation else {
                    return true
                }
                return location.distance(from: lastLocation) > self.kDistanceTolerance
            })
            .do(onNext: { [weak self] location in
                self?.lastLocation = location
                self?.mapView.setRegion(MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 600, longitudinalMeters: 600), animated: true)
                self?.mapView.setCenter(location.coordinate, animated: true)
            })
            .map { location -> MapCommand in
                return MapCommand.loadRestaurants(location)
            }
            .bind(to: outputSubject)
            .disposed(by: disposeBag)
    }
    
    private func configureAnnotations(forState state: Observable<MapState>) -> Observable<[MapAnnotation]> {
        return state
            .do(onNext: { [weak self] state in
//                state.isLoading ? self?.startLoading() : self?.stopLoading()
                if let error = state.error {
                    self?.showErrorMessage(title: "Error", message: error.message)
                }
            })
            .map({ (state) -> [MapAnnotation] in
                var annotations: [MapAnnotation] = []
                
                state.restaurantList?.nearbyRestaurants.forEach({ restaurant in
                    if let coordinate = CLLocationCoordinate2D(latitude: restaurant.location.latitude, longitude: restaurant.location.longitude) {
                        annotations.append(
                            MapAnnotation(title: restaurant.name,
                                          locationName: restaurant.location.address,
                                          imageURL: restaurant.featuredImage,
                                          coordinate: coordinate)
                        )
                    }
                })
                return annotations
            })
    }
}

