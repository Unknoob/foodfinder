//
//  MapInteractor.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import RxSwift

protocol MapInteracting {
    var initialState: MapState { get set }
    var dataFetcher: LocationRestaurantFetching { get }
    func execute(command: Observable<MapCommand>) -> Observable<MapState>
    func mutation(for command: MapCommand) -> Observable<MapMutation>
    func reduce(state: MapState, mutation: MapMutation) -> MapState
}

class MapInteractor: MapInteracting {
    var initialState: MapState
    var dataFetcher: LocationRestaurantFetching
    
    init(initialState: MapState, dataFetcher: LocationRestaurantFetching) {
        self.initialState = initialState
        self.dataFetcher = dataFetcher
    }
    
    func execute(command: Observable<MapCommand>) -> Observable<MapState> {
        let lastCommand = ReplaySubject<MapCommand>.create(bufferSize: 1)
        let lastState = ReplaySubject<MapState>.create(bufferSize: 1)
        
        return command
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .userInitiated))
            .flatMap { command -> Observable<MapMutation> in
                return self.mutation(for: command)
                    .do(onNext: { _ in
                        lastCommand.onNext(command)
                    })
            }
            .scan(initialState, accumulator: reduce)
            .startWith(initialState)
            .do(onNext: lastState.onNext)
    }
    
    func mutation(for command: MapCommand) -> Observable<MapMutation> {
        switch command {
        case let .loadRestaurants(location):
            return dataFetcher.locateRestaurants(near: location)
                .map({ result -> MapMutation in
                    switch result {
                    case let .success(restaurantList):
                        return MapMutation.loadSuccess(restaurantList)
                    case let .failure(error):
                        return MapMutation.error(error)
                    }
                })
                .startWith(MapMutation.loading)
        default:
            return .just(MapMutation.loading)
        }
    }
    
    func reduce(state: MapState, mutation: MapMutation) -> MapState {
        var newState = state
        newState.isLoading = false
        newState.error = nil
        switch mutation {
        case let .loadSuccess(restaurantList):
            if newState.restaurantList != nil {
                newState.restaurantList?.nearbyRestaurants.append(contentsOf: restaurantList.nearbyRestaurants)
            } else {
                newState.restaurantList = restaurantList
            }
        case let .refreshSuccess(restaurantList):
            newState.restaurantList = restaurantList
        case let .error(error):
            newState.error = error
        default:
            break
        }
        
        return newState
    }
}
