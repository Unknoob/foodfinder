//
//  MapMutation.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

enum MapMutation: Equatable {
    case refreshSuccess(RestaurantList)
    case loadSuccess(RestaurantList)
    case error(GenericError)
    case loading
    
    static func == (lhs: MapMutation, rhs: MapMutation) -> Bool {
        switch (lhs, rhs) {
        case (let .refreshSuccess(lhsRestaurantList), let .refreshSuccess(rhsRestaurantList)):
            return lhsRestaurantList == rhsRestaurantList
        case (let .loadSuccess(lhsRestaurantList), let .loadSuccess(rhsRestaurantList)):
            return lhsRestaurantList == rhsRestaurantList
        case (let .error(lhsError), let .error(rhsError)):
            return lhsError.message == rhsError.message
        case (.loading, .loading):
            return true
        default:
            return false
        }
    }
}
