//
//  MapCommand.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import CoreLocation

enum MapCommand: Equatable {
    case refresh
    case showDetail(String)
    case loadRestaurants(CLLocation)
}
