//
//  MapState.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import MapKit

struct MapState: Equatable {
    var location: CLLocation?
    var restaurantList: RestaurantList?
    var error: GenericError?
    var isLoading: Bool
    
    init() {
        self.isLoading = true
    }
    
    static func == (lhs: MapState, rhs: MapState) -> Bool {
        return (lhs.restaurantList == rhs.restaurantList) && (lhs.isLoading == rhs.isLoading) && (lhs.location == rhs.location)
    }
}
