//
//  LocationRestaurantFetcher.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Alamofire
import CoreLocation
import RxSwift

protocol LocationRestaurantFetching {
    func locateRestaurants(near: CLLocation) -> Observable<Result<RestaurantList>>
}

class LocationRestaurantFetcher: LocationRestaurantFetching {
    func locateRestaurants(near location: CLLocation) -> Observable<Result<RestaurantList>> {
        return NetworkManager.apiCall(requestPath: "geocode",
                                      parameters: ["lat": location.coordinate.latitude,
                                                   "long": location.coordinate.longitude],
                                      httpMethod: Alamofire.HTTPMethod.get)
    }
}
