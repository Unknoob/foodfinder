//
//  MapAnnotation.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import MapKit

class MapAnnotation: NSObject, MKAnnotation {
    var title: String?
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    var imageURL: String?
    
    init(title: String, locationName: String, imageURL: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.imageURL = imageURL
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
