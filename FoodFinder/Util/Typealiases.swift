//
//  Typealiases.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

/// A normal Dictionary.
public typealias JSONDictionary = [String: Any]
/// An array of Dictionaries.
public typealias JSONArray = [JSONDictionary]
