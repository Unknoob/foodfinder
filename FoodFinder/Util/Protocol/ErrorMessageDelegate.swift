//
//  ErrorMessageDelegate.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import UIKit

protocol ErrorMessageDelegate {
    func showErrorMessage(title: String, message: String)
}

extension ErrorMessageDelegate where Self: UIViewController {
    func showErrorMessage(title: String, message: String) {
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default))
        
        self.present(alertController, animated: true, completion: nil)
    }

}
