//
//  GenericError.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

public protocol GenericError: Error {
    var title: String { get }
    var message: String { get }
}
