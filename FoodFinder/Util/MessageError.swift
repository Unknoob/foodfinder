//
//  MessageError.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

enum MessageError: GenericError {
    case message(title: String, message: String)

    var title: String {
        switch self {
        case let .message(title, _):
            return title
        }
    }
    
    var message: String {
        switch self {
        case let .message(_, message):
            return message
        }
    }
}
