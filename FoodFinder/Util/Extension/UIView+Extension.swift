//
//  UIView+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import UIKit

extension UIView {
    static var className: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
