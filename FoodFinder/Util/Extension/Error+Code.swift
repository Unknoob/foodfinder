//
//  Error+Code.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Foundation

extension Error {
    var code: Int {
        get {
            return (self as NSError).code
        }
    }
}
