//
//  UIColor+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 14/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import UIKit

extension UIColor {
    static var dimmedScreen: UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
    }
}
