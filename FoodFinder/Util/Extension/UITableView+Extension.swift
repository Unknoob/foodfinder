//
//  UITableView+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import UIKit

extension UITableView {
    
    public func register<T: UITableViewCell>(_: T.Type) {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.className, bundle: bundle)
        
        self.register(nib, forCellReuseIdentifier: T.className)
    }
}
