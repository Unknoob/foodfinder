//
//  CLLocationCoordinate2D+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import MapKit

extension CLLocationCoordinate2D {
    init?(latitude: String, longitude: String) {
        guard let lat = Double(latitude), let long = Double(longitude) else {
            return nil
        }
        self.init(latitude: lat, longitude: long)
    }
}

extension CLLocationCoordinate2D: Equatable {
    public static func == (lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return (lhs.latitude == rhs.latitude) && (lhs.longitude == rhs.longitude)
    }
}
