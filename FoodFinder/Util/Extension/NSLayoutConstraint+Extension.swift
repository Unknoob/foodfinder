//
//  NSLayoutConstraint+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 14/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    @discardableResult
    public func activate() -> NSLayoutConstraint {
        self.isActive = true
        return self
    }
    
    @discardableResult
    public func deactivate() -> NSLayoutConstraint {
        self.isActive = false
        return self
    }
}
