//
//  String+Extension.swift
//  FoodFinder
//
//  Created by Gabe on 14/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import Foundation

extension String {
    func removingPunctuation() -> String {
        return self.replacingOccurrences(of: "(", with: "")
                .replacingOccurrences(of: ")", with: "")
                .replacingOccurrences(of: " ", with: "")
                .replacingOccurrences(of: "-", with: "")
    }

}
