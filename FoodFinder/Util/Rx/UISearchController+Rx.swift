//
//  UISearchController+Rx.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

class RxUISearchControllerDelegateProxy: DelegateProxy<UISearchController, UISearchResultsUpdating>, UISearchResultsUpdating, DelegateProxyType {
    
    static func currentDelegate(for object: UISearchController) -> UISearchResultsUpdating? {
        return object.searchResultsUpdater
    }
    
    static func setCurrentDelegate(_ delegate: UISearchResultsUpdating?, to object: UISearchController) {
        object.searchResultsUpdater = delegate
    }
    

    internal lazy var text = PublishSubject<String?>()

    private weak var searchController: UISearchController?

    init(searchController: ParentObject) {
        self.searchController = searchController
        super.init(parentObject: searchController, delegateProxy: RxUISearchControllerDelegateProxy.self)
    }

    func updateSearchResults(for searchController: UISearchController) {
        text.onNext(searchController.searchBar.text)
    }

    static func registerKnownImplementations() {
        self.register { RxUISearchControllerDelegateProxy(searchController: $0) }
    }
}

extension Reactive where Base: UISearchController {

    var delegate: DelegateProxy<UISearchController, UISearchResultsUpdating> {
        return RxUISearchControllerDelegateProxy.proxy(for: base)
    }

    var text: Observable<String?> {
        return (delegate as! RxUISearchControllerDelegateProxy).text.asObservable()
    }
}
