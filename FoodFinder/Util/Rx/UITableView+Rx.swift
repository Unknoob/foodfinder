//
//  UITableView+Rx.swift
//  FoodFinder
//
//  Created by Gabe on 13/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: UITableView {
    
    var reachedBottom: Observable<Bool> {
        let weakTableView = base
        return base.rx.willDisplayCell
            .map { $0.indexPath }
            .map { indexPath in
                indexPath.section == (weakTableView.numberOfSections - 1) &&
                    indexPath.row == (weakTableView.numberOfRows(inSection: indexPath.section) - 1)
            }
            .distinctUntilChanged()
            .filter { $0 }
            .debounce(.seconds(2), scheduler: MainScheduler.asyncInstance)
    }
    
    func deselectRowAtIndexPath(animated: Bool) -> AnyObserver<IndexPath> {
        return Binder(base) { tableView, indexPath in
            tableView.deselectRow(at: indexPath, animated: animated)
        }
        .asObserver()
    }
}
