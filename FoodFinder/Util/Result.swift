//
//  Result.swift
//  FoodFinder
//
//  Created by Gabe on 12/09/19.
//  Copyright © 2019 Gabecode. All rights reserved.
//

/// An enum containing the possible results of a request.
/// - success: Returns a Value specified on implementation.
/// - failure: Returns a GenericError.
public enum Result<Value> {
    /// Returns a value specified on implementation.
    case success(Value)
    /// Returns a GenericError.
    case failure(GenericError)
}
